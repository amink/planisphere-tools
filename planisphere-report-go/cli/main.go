package main

import "gitlab.oit.duke.edu/devil-ops/planisphere-tools/planisphere-report-go/cli/cmd"

func main() {
	cmd.Execute()
}
