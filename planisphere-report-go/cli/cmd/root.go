package cmd

import (
	"os"
	"strings"
	"time"

	"github.com/mitchellh/go-homedir"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"

	"github.com/spf13/viper"
)

var cfgFile string

var (
	planisphereKey string
	planisphereURL string
	version        = "dev"
	startedAt      time.Time
)

// Verbose Logging
var Verbose bool

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "planisphere-report",
	Short:   "Self report tool for Planisphere",
	Long:    `Self report tool for Planisphere. Currently supported on macOS, Linux and FreeBSD`,
	Version: version,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		// Key is gonna be required
		startedAt = time.Now()
		planisphereKey = viper.GetString("key")
		if planisphereKey == "" {
			log.Fatal().Msg("Must set your Planisphere Key in the config file or env. See README.md for full details. Shortcut to link to your keys: https://duke.is/vywtw")
		}
		log.Debug().Str("planisphere_key", planisphereKey).Msg("Using Key")

		// Set URL if needed
		planisphereURL = viper.GetString("url")
		if planisphereURL == "" {
			planisphereURL = "https://planisphere.oit.duke.edu/self_report"
		}
		log.Debug().Str("url", planisphereURL).Msg("Using URL")
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.planisphere-report.yaml)")
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "Enable verbose output")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	log.Logger = log.Output(zerolog.ConsoleWriter{
		Out:        os.Stderr,
		TimeFormat: "2006-01-02T15:04:05.999Z07:00",
	})
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if Verbose {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Now look in /etc
		viper.AddConfigPath("/etc/")
		viper.SetConfigName("planisphere-report")
		err = viper.ReadInConfig()
		log.Debug().Err(err).Msg("Error reading config")

		// Search config in home directory with name ".planisphere-report" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".planisphere-report")
		err = viper.MergeInConfig()
		log.Debug().Err(err).Msg("Error doing merge config")

		// If no key is yet set, load it in from a file
		if viper.GetString("key") == "" {
			possibleKeyFiles := []string{"/etc/planisphere_key_file", "/etc/planisphere-report-key"}
			for _, pkf := range possibleKeyFiles {
				dat, err := os.ReadFile(pkf)
				if err == nil {
					viper.Set("key", strings.TrimSpace(string(dat)))
					break
				}
			}
		}
		if viper.GetString("key") == "" {
			log.Fatal().Msg(`Could not find your planisphere key. Please set it in one of the following ways:

In your planisphere-report.yaml file, use:

Option 1:

---
key: your-key

Option 2:

Set the key contents in either  /etc/planisphere_key_file or /etc/planisphere-report-key

Option 3:

Use an environment variable like:

$ export PLANISPHERE_REPORT_KEY=your-key`)
		}
	}

	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvPrefix("planispherereport")

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Debug().Str("config_file", viper.ConfigFileUsed()).Msg("Using config file")
	}
}
